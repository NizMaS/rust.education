use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    // println!("x = {x} and y + 2 = {}", y + 2);
    println!("Игра угадай число!");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        println!("Плез, введите число:");

        let mut guess = String::new(); // изменяемая
        io::stdin()
            .read_line(&mut guess) // & указывает, что этот аргумент является ссылкой
            .expect("Failed to read line");


        // затенение переменной
        let guess: u32 = match guess.trim()           // удалит любые пробельные символы 
                                .parse() {           // преобразует строку в другой тип
                                    Ok(num) => num,
                                    Err(_) => {
                                        println!("Не удалось преобразовать число {guess}");
                                        continue;
                                    },
                                };
                                // .expect("Please type a number!");           // Выбрасывает Panic (типа Exception)

        println!("You guessed: {guess}");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("Бинго! вы угадали!");
                break;                                  // заставляет программу выйти из цикла,
            }
        }
    }
}

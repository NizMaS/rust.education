use std::{ env, process};
use chrono::prelude::Utc;
use minigrep::Config;

fn main() {
    println!("***Start {}***", Utc::now().to_string());

    // материализуем список аргументов
    // let args: Vec<String> = env::args().collect();

    let config = Config::build(env::args()).unwrap_or_else(
        // анонимная функция
        |err| {
            eprintln!("Problem parsing arguments: {err}");
            process::exit(1);
    });

    println!("Searching for \"{}\" In file \"{}\"", config.query, config.file_path);

    if let Err(e) = minigrep::run(config){
        eprintln!("Application error: {e}");
        process::exit(1);
    };
    
    println!("***End {}***", Utc::now().to_string());
}

